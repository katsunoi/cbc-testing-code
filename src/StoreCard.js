import React from 'react';


const StoreCard = ({address, id}) => {
	  return (
		      <div style={{border: 'black solid 1px'}}>
						      <p className="address-field" style={{color: 'palevioletred'}}>{address || 'this should be an address'}</p>
							    <p className="id-field">StoreID: {id || 'this should be a store ID'}</p>
					</div>
		    )
}

export default StoreCard;