import StoreList from '../StoreList';
import Adapter from 'enzyme-adapter-react-16';
import toJson from 'enzyme-to-json';
import React from 'react';
import Enzyme, { shallow, mount } from 'enzyme';
Enzyme.configure({ adapter: new Adapter() });

describe('<StoreList />', () => {
	const props= {

		stores : [
								{ id: 0, address: '123 West Wood Tree' }, 
								{ id: 1, address: '456 Cool Park Styx' }, 
								{ id: 2, address: '789 Red Rum Mine' }
							],
		sale: false		
	};

	it('should render properly', () => {
		const wrapper = shallow(<StoreList { ...props } />);
	});

	it("should render a button", () => {
		const wrapper = shallow(<StoreList 
																stores={props.stores} 
																sale={props.sale}
																loadData={ jest.fn() }
																/>
														);
		expect(wrapper.find('button').exists()).toBe(true);
	});

	it("should call loadData prop when button is clicked", () => {
		const wrapper = mount(<StoreList 
																stores={props.stores} 
																sale={props.sale}
																loadData={ jest.fn() }
																/>
														);	
		const button = wrapper.find('button');
		button.simulate('click');	
		expect(wrapper.props().loadData.mock.calls.length).toEqual(1);
	});

	it("should render a list of StoreCards from the stores prop", () => {
		const wrapper = shallow(<StoreList 
																stores={props.stores} 
																sale={props.sale}
																loadData={ jest.fn() }
																/>
														);	
		const cards = wrapper.find('StoreCard');
		expect(cards.length).toEqual(props.stores.length);
	});

	it('should apply a class of sale based on sale prop', () => {
		const wrapper = shallow(<StoreList 
																stores={props.stores} 
																sale={true}
																loadData={ jest.fn() }
																/>
														);	
		// find the outermost div
		const div = wrapper.find('div').first();
		expect(div.hasClass('special')).toBe(true);
	});

	it('should match the snapshot', ()=>{
		const wrapper = shallow(<StoreList 
																stores={props.stores} 
																sale={props.sale}
																loadData={ jest.fn() }
																/>
														);	

		expect(toJson(wrapper)).toMatchSnapshot();
	});
});
