import StoreCard from '../StoreCard';
import Adapter from 'enzyme-adapter-react-16';
import toJson from 'enzyme-to-json';
import React from 'react';
import Enzyme, { shallow, mount } from 'enzyme';
Enzyme.configure({ adapter: new Adapter() });


describe('<StoreCard />', () => {
  it('renders without error', () => {
    const wrapper = shallow(<StoreCard id={1} address="23 Blue Jays Way"/>);
    expect(wrapper).toBeTruthy();
  });

  it('renders the address prop in a p with class address-field', () => {
    const wrapper = shallow(<StoreCard id={1} address="23 Blue Jays Way"/>);
    const addressField = wrapper.find('p.address-field');
    expect(addressField.text()).toEqual("23 Blue Jays Way");
  });

  it('renders the id prop in a p with class id-field', () => {
    const wrapper = shallow(<StoreCard id={1} address="23 Blue Jays Way"/>);
    const idField = wrapper.find('p.id-field');
    expect(idField.text()).toEqual("StoreID: 1");
  });

  it('should match the snapshot', () => {
    const wrapper = shallow(<StoreCard id={1} address="23 Blue Jays Way"/>);
    expect(toJson(wrapper)).toMatchSnapshot();
  });
});