import React from 'react';
import MyComponent from '../MyComponent';
import renderer from 'react-test-renderer';
import Adapter from 'enzyme-adapter-react-16';
import toJson from 'enzyme-to-json';
import Enzyme, { shallow, mount } from 'enzyme';
Enzyme.configure({ adapter : new Adapter() })

// use describe block to group tests togeter 
describe('<MyComponent />', () => {
    // test # 1
  it('renders without error', () => {
    let component = renderer.create(<MyComponent />);
    expect(component).toBeTruthy();
  })

    // test # 2
  it('matches the snapshot', () => {
    let component = renderer.create(<MyComponent />);
    expect(toJson(component)).toMatchSnapshot();
  }) 
});

  it('renders without error', () => {
    // step 1: render the component
    let component = renderer.create(<MyComponent />);

    // step 2: interrogate it to test the intended behaviour
    expect(component).toBeTruthy();
  })

  it('renders <MyChild> as a child', ()=> {
    let wrapper = mount(<MyComponent/>);
    let myChild = wrapper.find('MyChild');
    let grandChild = myChild.find('GrandChild');
  })