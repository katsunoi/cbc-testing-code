import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import StoreList from './StoreList';
const storesArray = [
  { id: 0, address: '123 West Wood Tree' }, 
  { id: 1, address: '456 Cool Park Styx' }, 
  { id: 2, address: '789 Red Rum Mine' }
];

class App extends Component {
  render() {
    return (
      <div className="App">
        <StoreList 
            stores={storesArray}
            sale={true}
            loadData={()=>{console.log('fetching data')}}
            />
      </div>
    );
  }
}

export default App;
