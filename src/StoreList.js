import React from "react";
import StoreCard from './StoreCard';

const StoreList = ({stores, sale, loadData}) => {
	return (
		<div className={ sale ? 'special': ''}>
			{ stores.map((item, index) => {
					return (<StoreCard 
						key={item.id} 
						address={item.address} 
						id={item.id}
						/>  )

				}) }
			<button onClick={loadData}>
					load more stores
			</button>
		</div>
		    )
}

export default StoreList;