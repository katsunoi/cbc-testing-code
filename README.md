# CBC Example Jest + Enzyme Code for React Unit Testing.

This repo contains a create-react-app that has two components and some example tests for those components:

- StoreCard
	- renders one card for a single store based off of props passed in
- StoreList
	- renders a list of StoreCard components based on an array of store objects passed in through the 'store' prop
	- toggles a class of 'special' on the containing div based on the boolean value passed in through the 'sale' prop
	- renders a button that, when clicked, fires whatever function was passed in through the 'loadData' prop

	
## instructions
 - use npm start to run the project in dev mode
 - use npm run test to run the component unit tests
